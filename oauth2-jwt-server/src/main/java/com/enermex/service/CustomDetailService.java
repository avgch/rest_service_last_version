package com.enermex.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import com.enermex.repository.UsuarioRepository;
import com.enermex.dto.Usuario;

@Service
public class CustomDetailService implements UserDetailsService{

	@Autowired
	private UsuarioRepository usuarioRepository;
	

	@Override
	public UserDetails loadUserByUsername(String correo) throws UsernameNotFoundException {
		// TODO Auto-generated method stub
	
		
		// TODO Auto-generated method stub
				 Usuario user = usuarioRepository.findByCorreo(correo);
				 List<GrantedAuthority> authorities = new ArrayList<>();
	    
			
				 authorities.add(new SimpleGrantedAuthority("ROLE_ADMINISTRADOR"));
				 authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
				UserDetails userDet = new User(user.getCorreo(),user.getContrasena(),authorities) ;
				 
				return userDet;
	}

}
