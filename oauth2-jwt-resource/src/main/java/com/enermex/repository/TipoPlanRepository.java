/**
 * 
 */
package com.enermex.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.enermex.dto.TipoPlanDto;

/**
 * @author wkn-40
 *
 */

@Repository
@Transactional
public interface TipoPlanRepository extends  JpaRepository<TipoPlanDto, Integer> {

}
