package com.enermex.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.enermex.dto.PipaDto;;

@Repository
@Transactional
public interface PipasRepository extends JpaRepository<PipaDto, Long> {
	
	
	
	List<PipaDto> findAll();
	
	@Query("Select p from PipaDto p where p.idPipa = ?1")
	PipaDto findPipaById(Integer id);
	
	
	@Modifying
	@Query("update PipaDto p set p.placas=?1, p.modelo=?2, p.marca=?3  where p.idPipa = ?4")
	void updatePipa(String placa,String modelo,String marca,Integer idPipa);
	
}
