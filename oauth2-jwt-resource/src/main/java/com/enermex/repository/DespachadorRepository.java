package com.enermex.repository;

import java.util.List;

import com.enermex.dto.DespachadorDto;

import org.springframework.data.repository.CrudRepository;
import org.springframework.lang.Nullable;

/**
 * Repositorio de tabla Despachador
 * @author José Antonio González Hernández
 * 
 * Contacto: agonzalez@chromaticus.mx
 * Fecha de creación: 27 de diciembre de 2019
 */
public interface DespachadorRepository extends CrudRepository<DespachadorDto, Long> {
    /**
     * Permite obtener los despachadores sin cierto estado; con lo que se
     * pueden ignorar los pseudoeliminados.
     * 
     * @param status Estado ignorado para el conjunto de despachadores
     * @return Lista de despachadores
     */
    List<DespachadorDto> findAllByStatusNot(Integer status);

    @Nullable
    DespachadorDto findByIdDespachadorAndStatusNot(Long despachador, Integer status);
}