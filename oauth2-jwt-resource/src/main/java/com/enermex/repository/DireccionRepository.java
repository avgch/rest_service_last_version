package com.enermex.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.enermex.dto.DireccionDto;

public interface DireccionRepository extends JpaRepository<DireccionDto, Long> {

	
	@Modifying
	@Query("update DireccionDto d set d.calle=?1, d.cp=?2, d.colonia=?3, d.latitud=?4, d.longitud=?5   where d.idDireccion = ?6")
	void updateDireccion(String calle,String cp,String colonia,double latitud, double longitud, Integer idDireccion);
}
