package com.enermex.repository;


import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;


import com.enermex.dto.UsuarioDto;


@Repository
@Transactional
public interface UsuarioRepository  extends JpaRepository<UsuarioDto, Long>{
	
	
	
	List<UsuarioDto> findAll();
	
	
	UsuarioDto findByIdUsuario(Integer idUsuario);
	
	@Modifying
	@Query("update UsuarioDto u set u.nombre=?1,u.apellidoPaterno=?2, u.apellidoMaterno=?3, u.estadoCivil=?4, u.correo = ?5, u.telefono = ?6  where u.idUsuario = ?7")
	void updateUsuario(String nombre,String apellidoPaterno,String apellidoMaterno,String estadoCivil, String correo, String telefono, Integer usuarioId);
	
	@Modifying
	@Query("update UsuarioDto u set u.estatus=2 where u.idUsuario = ?1")
	void delete(Integer usuarioId);
	
	
	@Modifying
	@Query("update UsuarioDto u set u.isId=?1  where u.idUsuario = ?2")
	void updateIsid(Integer isId,Integer id);
	

	@Query("Select u from UsuarioDto u where u.correo = ?1")
	UsuarioDto findByEmail(String email);
	
	
	@Modifying
	@Query("update UsuarioDto u set u.contrasena=?1  where u.idUsuario = ?2 and u.correo=?3")
	void updatePassword(String contrasena,Integer id,String correo);

}
