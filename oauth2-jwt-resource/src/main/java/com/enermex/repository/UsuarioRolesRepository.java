package com.enermex.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.enermex.dto.UsuarioRoles;

public interface UsuarioRolesRepository extends JpaRepository<UsuarioRoles, Long>{

}
