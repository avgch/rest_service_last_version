package com.enermex.service;

import java.util.Date;
import java.util.List;

import com.enermex.dto.DespachadorDto;
import com.enermex.dto.DireccionDto;
import com.enermex.repository.DespachadorRepository;
import com.enermex.repository.DireccionRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Servicio de operaciones en la tabla Despachador
 * @author José Antonio González Hernández
 * 
 * Contacto: agonzalez@chromaticus.mx
 * Fecha de creación: 30 de diciembre de 2019
 */
@Service
public class DespachadorService {
    Logger logger = LoggerFactory.getLogger(DespachadorService.class);

    @Autowired
    private DespachadorRepository despachadorRepository;
    
    @Autowired
    private DireccionRepository direccionRepository;
    
    @Transactional
    public boolean createDespachador(DespachadorDto despachador){
        if(exists(despachador)){
            logger.error(
                "Trata de añadir un despachador existente - ID: " +
                despachador.getIdDespachador());
            
            return false;
        }

        // Se establece el estado (Por verificar) y la fecha de creación
        Date date = new Date();
        despachador.setFechaIngreso(date);
        despachador.setStatus(3);

        // TODO: Definir el identificador interno
        despachador.setInternalId("");
        
        for (DireccionDto direccionDto: despachador.getDirecciones()) {
            direccionDto.setFechaCreacion(date);
        }

        return saveDespachador(despachador);
    }

    public boolean updateDespachador(DespachadorDto despachador){
        if(!exists(despachador)){
            logger.error(
                "Trata de actualizar un despachador inexistente - ID: " +
                despachador.getIdDespachador());
            
            return false;
        }

        return saveDespachador(despachador);
    }

    public boolean deleteDespachador(DespachadorDto despachador) {
        if(!exists(despachador)){
            logger.error(
                "Trata de eliminar un despachador inexistente - ID: " +
                despachador.getIdDespachador());
            
            return false;
        }

        despachador.setStatus(2);
        return saveDespachador(despachador);
    }

    @Transactional(readOnly = true)
    public List<DespachadorDto> queryAll(boolean includeDeleted){
        int status = includeDeleted ? -1 : 2;

        try {
            return despachadorRepository.findAllByStatusNot(status);
        } catch(Exception e) {
            logger.error("Error al obtener despachadores: " + e.getMessage());
            return null;
        }
    }

    @Transactional(readOnly = true)
    public DespachadorDto queryById(Long id, boolean includeDeleted){
        int status = includeDeleted ? -1 : 2;

        try {
            return despachadorRepository
                .findByIdDespachadorAndStatusNot(id, status);
        } catch(Exception e) {
            logger.error("Error al obtener un despachador: " + e.getMessage());
            return null;
        }
    }

    @Transactional(readOnly = true)
    private boolean exists(DespachadorDto despachador) {
        try{
            return despachador != null &&
                despachador.getIdDespachador() != null &&
                despachadorRepository.existsById(despachador.getIdDespachador());
        } catch (Exception e) {
            logger.error("Error al comprobar si existe un despachador: " + e.getMessage());
            return false;
        }
    }

    @Transactional
    private boolean saveDespachador(DespachadorDto despachador) {
        try {
            despachadorRepository.save(despachador);
            return true;
        } catch (Exception e) {
            logger.error("Error al guardar un despachador: " + e.getMessage());

            e.printStackTrace();
            return false;
        }
    }
}
