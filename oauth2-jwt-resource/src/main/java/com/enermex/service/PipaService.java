package com.enermex.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;
import com.enermex.repository.PipasRepository;


import com.enermex.dto.PipaDto;

@Service
public class PipaService {

	
	@Autowired
	PipasRepository pipasRepository;
	
	
	public List<PipaDto> getPipas(){
		
		
		return pipasRepository.findAll();
	}
	public void saveUpdate(PipaDto pipa) {
		
		pipa.setFechaCreacion(new Date());
		pipa.setIdCombustible(1);
		this.pipasRepository.save(pipa);
		
	}
	
	public PipaDto findPipaById(Integer id) {
		
		return pipasRepository.findPipaById(id);
		
	}


	public void update(PipaDto pipa) {
		
		pipa.setFechaCreacion(new Date());
		this.pipasRepository.updatePipa(pipa.getPlacas(),pipa.getModelo(),pipa.getMarca(),pipa.getIdPipa());
		
	}
	
	
}
