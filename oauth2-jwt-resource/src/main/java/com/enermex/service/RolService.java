package com.enermex.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enermex.dto.RolDto;
import com.enermex.repository.RolRepository;

@Service
public class RolService {
	
	
	@Autowired
	private RolRepository rolRepository;
	
	public List<RolDto> getAll(){
		
		return rolRepository.findAll();
	
	}

}
