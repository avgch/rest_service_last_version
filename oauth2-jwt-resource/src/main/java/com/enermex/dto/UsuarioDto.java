/*##############################################################################


# Nombre del Programa : UsuarioDto.java                                        #
# Autor               : Abraham Vargas                                         #
# Compania            : Enermex                                                #
# Proyecto :                                                 Fecha: 17/02/2017 #
# Descripcion General :                                                        #
# Programa Dependiente:                                                        #
# Programa Subsecuente:                                                        #
# Cond. de ejecucion  :                                                        #
# Dias de ejecucion   :                                      Horario: hh:mm    #
#                              MODIFICACIONES                                  #
################################################################################
# Autor               :                                                        #
# Compania            :                                                        #
# Proyecto/Procliente :                                      Fecha:            #
# Modificacion        :                                                        #
# Marca de cambio     :                                                        #
################################################################################
# Numero de Parametros:                                                        #
# Parametros Entrada  :                                      Formato:          #
# Parametros Salida   :                                      Formato:          #
##############################################################################*/


package com.enermex.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.enermex.dto.TipoPlanDto;

import org.springframework.stereotype.Component;

@Entity
@Table(name = "usuarios")
@Component
public class UsuarioDto implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_usuario")
	private Integer idUsuario;
	
	@Column(name="nombre")
	private String nombre;
	
	@Column(name="apellido_paterno")
	private String apellidoPaterno;
	
	@Column(name="apellido_materno")
	private String apellidoMaterno;
	
	@Column(name="correo")
	private String correo;
	
	@Column(name="telefono")
	private String telefono;
	
	@Column(name="contrasena")
	private String contrasena;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="fecha_creacion")
	private Date fechaCreacion;
	
	@Column(name = "id_tipo_usuario")
	private Long idTipoUsuario;
	
	@Column(name = "estatus")
	private Integer estatus; 
	
	@Transient
	private Integer idPlan;
	
	@Column(name = "genero")
	private String genero;
	
	@Column(name = "isid")
	private Integer isId;
	
	@Column(name = "estado_civil")
	private String estadoCivil;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="fecha_nacimiento")
	private Date fechaNacimiento;
	
	@JoinTable(
	        name = "usuario_tipo_plan",
	        joinColumns = @JoinColumn(name = "id_usuario", nullable = false),
	        inverseJoinColumns = @JoinColumn(name="id_tipo_plan", nullable = false)
	    )
	@ManyToMany(cascade = CascadeType.ALL)
    private List<TipoPlanDto> planes;
	
	 
	@OneToMany(mappedBy="usuario")
    private List<DireccionDto> direccion;
	
	@JoinTable(
	        name = "usuarios_rol",
	        joinColumns = @JoinColumn(name = "id_usuario", nullable = false),
	        inverseJoinColumns = @JoinColumn(name="id_rol", nullable = false)
	    )
	@ManyToMany(cascade = CascadeType.ALL)
	 private List<RolDto> roles ;
	
	
	
	public UsuarioDto() {

	}
	

	
	public Integer getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getContrasena() {
		return contrasena;
	}
	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Long getIdTipoUsuario() {
		return idTipoUsuario;
	}

	public void setIdTipoUsuario(Long idTipoUsuario) {
		this.idTipoUsuario = idTipoUsuario;
	}


	public List<RolDto> getRoles() {
		return roles;
	}



	public void setRoles(List<RolDto> roles) {
		this.roles = roles;
	}



	public List<DireccionDto> getDireccion() {
		return direccion;
	}



	public void setDireccion(List<DireccionDto> direccion) {
		this.direccion = direccion;
	}



	public Integer isEstatus() {
		return estatus;
	}



	public void setEstatus(Integer estatus) {
		this.estatus = estatus;
	}



	public List<TipoPlanDto> getPlanes() {
		return planes;
	}



	public void setPlanes(List<TipoPlanDto> planes) {
		this.planes = planes;
	}



	public Integer getIdPlan() {
		return idPlan;
	}



	public void setIdPlan(Integer idPlan) {
		this.idPlan = idPlan;
	}



	public String getGenero() {
		return genero;
	}



	public void setGenero(String genero) {
		this.genero = genero;
	}



	public String getEstadoCivil() {
		return estadoCivil;
	}



	public void setEstadoCivil(String estadoCivil) {
		this.estadoCivil = estadoCivil;
	}



	public Integer getEstatus() {
		return estatus;
	}



	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}



	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}



	public String getApellidoPaterno() {
		return apellidoPaterno;
	}



	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}



	public String getApellidoMaterno() {
		return apellidoMaterno;
	}



	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}



	public Integer getIsId() {
		return isId;
	}



	public void setIsId(Integer isId) {
		this.isId = isId;
	}
  
	
	

    
	

    
	

}
