package com.enermex.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "servicios")
public class ServicioDto {
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id_servicio")
	private Integer idServicios;
	
	
	@Column(name = "servicio")
	private String servicio;
	
	@Column(name = "precio")
	private float precio;
	
		

	public Integer getIdServicios() {
		return idServicios;
	}

	public void setIdServicios(Integer idServicios) {
		this.idServicios = idServicios;
	}

	public String getServicio() {
		return servicio;
	}

	public void setServicio(String servicio) {
		this.servicio = servicio;
	}

	public float getPrecio() {
		return precio;
	}

	public void setPrecio(float precio) {
		this.precio = precio;
	}
	
	
	
	
	
	
	
	
	
	
	

}
