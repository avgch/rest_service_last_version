package com.enermex.dto;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

/**
 * DTO de tabla Despachador
 * @author José Antonio González Hernández
 * 
 * Contacto: agonzalez@chromaticus.mx
 * Fecha de creación: 27 de diciembre de 2019
 */
@Entity
@Table(name="despachador")
public class DespachadorDto {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_despachador")
    private Long idDespachador;

    @Column(name="nombre")
    private String nombre;

    @Column(name="fecha_nac")
    private Date fechaNac;

    @Column(name="fecha_ingreso")
    private Date fechaIngreso;

    @Column(name="correo")
    private String correo;

    @Column(name="telefono")
    private String telefono;

    @Column(name="status")
    private Integer status;

    @Column(name="apellido_paterno")
    private String apellidoPaterno;

    @Column(name="apellido_materno")
    private String apellidoMaterno;
    
    // REQ: Kevin Villa
    // Utilizar una nomenclatura de identificación; postpuesto
    @Column(name="internal_id")
    private String internalId;

    @Column(name="estado_civil")
    private String estadoCivil;

    @Column(name="genero")
    private String genero;

    // REQ: Abraham Vargas
    // Al ingresar un despachador, debe especificarse su dirección
    @JoinTable(
        name = "despachador_direccion",
        joinColumns = @JoinColumn(name = "id_despachador"),
        inverseJoinColumns = @JoinColumn(name="id_direccion")
    )
    @ManyToMany(cascade = CascadeType.ALL)
    private List<DireccionDto> direcciones;

    public Long getIdDespachador() {
        return idDespachador;
    }

    public void setIdDespachador(Long idDespachador) {
        this.idDespachador = idDespachador;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Date getFechaNac() {
        return fechaNac;
    }

    public void setFechaNac(Date fechaNac) {
        this.fechaNac = fechaNac;
    }

    public Date getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(Date fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }
    
    public String getInternalId() {
        return internalId;
    }
    
    public void setInternalId(String internalId) {
        this.internalId = internalId;
    }

    public String getEstadoCivil() {
        return estadoCivil;
    }
    
    public void setEstadoCivil(String estadoCivil) {
        this.estadoCivil = estadoCivil;
    }
    
    public String getGenero() {
        return genero;
    }
    
    public void setGenero(String genero) {
        this.genero = genero;
    }
    
    public List<DireccionDto> getDirecciones() {
        return direcciones;
    }

    public void setDirecciones(List<DireccionDto> direcciones) {
        this.direcciones = direcciones;
    }
}