package com.enermex.dto;



import java.util.List;

import org.springframework.stereotype.Component;


@Component
public class ResponseDto<T> {

	
	private String code;
	
	private String message;
	
    private List<T> lista;
	
    
	
	public List<T> getLista() {
		return lista;
	}


	public void setLista(List<T> lista) {
		this.lista = lista;
	}


	public ResponseDto() {}
	

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	
	
	
	
	

}
