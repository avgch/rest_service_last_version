
/*##############################################################################
# Nombre del Programa : UsuarioTipoPlan.java                                   #
# Autor               : Abraham Vargas                                         #
# Compania            : Enermex                                                #
# Proyecto :                                                 Fecha: 17/02/2017 #
# Descripcion General :                                                        #
# Programa Dependiente:                                                        #
# Programa Subsecuente:                                                        #
# Cond. de ejecucion  :                                                        #
# Dias de ejecucion   :                                      Horario: hh:mm    #
#                              MODIFICACIONES                                  #
################################################################################
# Autor               :                                                        #
# Compania            :                                                        #
# Proyecto/Procliente :                                      Fecha:            #
# Modificacion        :                                                        #
# Marca de cambio     :                                                        #
################################################################################
# Numero de Parametros:                                                        #
# Parametros Entrada  :                                      Formato:          #
# Parametros Salida   :                                      Formato:          #
##############################################################################*/




package com.enermex.dto;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import com.enermex.dto.UsuarioTipoPlanKey;

import org.springframework.stereotype.Component;

@Component
@Entity
@Table(name = "usuario_tipo_plan")
public class UsuarioTipoPlan implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@EmbeddedId
	private UsuarioTipoPlanKey id;

	public UsuarioTipoPlanKey getId() {
		return id;
	}

	public void setId(UsuarioTipoPlanKey id) {
		this.id = id;
	}
	
	
	
	

}
