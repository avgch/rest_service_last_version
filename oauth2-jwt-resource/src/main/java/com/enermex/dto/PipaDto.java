package com.enermex.dto;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.stereotype.Component;


@Entity
@Table(name = "pipas")
@Component
public class PipaDto {

	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id_pipa")
	private Integer idPipa;
	
	@Column(name = "placas")
	private String placas;
	
	@Column(name = "marca")
	private String marca;
	
	
	@Column(name = "modelo")
	private String modelo;
	
	@Column(name = "fecha_creacion")
	@Temporal(value = TemporalType.TIMESTAMP)
	private Date fechaCreacion;
	
	@Column(name = "id_combustible")
	private Integer idCombustible;
	
	@Column(name = "estatus")
	private int estatus;
	
	
	public PipaDto() {
		
		
		
	}

    


	public Integer getIdPipa() {
		return idPipa;
	}




	public void setIdPipa(Integer idPipa) {
		this.idPipa = idPipa;
	}




	public String getPlacas() {
		return placas;
	}


	public void setPlacas(String placas) {
		this.placas = placas;
	}


	public String getMarca() {
		return marca;
	}


	public void setMarca(String marca) {
		this.marca = marca;
	}


	public String getModelo() {
		return modelo;
	}


	public void setModelo(String modelo) {
		this.modelo = modelo;
	}


	public Date getFechaCreacion() {
		return fechaCreacion;
	}


	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}




	public Integer getIdCombustible() {
		return idCombustible;
	}




	public void setIdCombustible(Integer idCombustible) {
		this.idCombustible = idCombustible;
	}




	public int getEstatus() {
		return estatus;
	}




	public void setEstatus(int estatus) {
		this.estatus = estatus;
	}
	
	
	
	
	
	
}
