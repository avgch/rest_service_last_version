/*##############################################################################
# Nombre del Programa : DireccionDto.java                                      #
# Autor               : Abraham Vargas                                         #
# Compania            : Enermex                                                #
# Proyecto :                                                 Fecha: 17/02/2017 #
# Descripcion General :                                                        #
# Programa Dependiente:                                                        #
# Programa Subsecuente:                                                        #
# Cond. de ejecucion  :                                                        #
# Dias de ejecucion   :                                      Horario: hh:mm    #
#                              MODIFICACIONES                                  #
################################################################################
# Autor               :                                                        #
# Compania            :                                                        #
# Proyecto/Procliente :                                      Fecha:            #
# Modificacion        :                                                        #
# Marca de cambio     :                                                        #
################################################################################
# Numero de Parametros:                                                        #
# Parametros Entrada  :                                      Formato:          #
# Parametros Salida   :                                      Formato:          #
##############################################################################*/

package com.enermex.dto;


import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;

import org.springframework.stereotype.Component;


@Component
@Entity
@Table(name = "direcciones")
public class DireccionDto  {
	


	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id_direccion")
	private Integer   idDireccion;
	
	@Column(name = "nombre")
	private String nombre;
	
	@Column(name = "cp")
	private String cp;
	
	@Column(name = "calle")
	private String calle;
	
	@Column(name = "colonia")
	private String colonia;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "fecha_creacion",columnDefinition = "DATETIME DEFAULT CURRENT_TIMESTAMP")
	private Date   fechaCreacion;
	
	@Column(name = "latitud")
	private double latitud;
	
	@Column(name = "longitud")
	private double longitud;

	 @ManyToOne
	 @JoinColumn(name = "id_usuario")
	private UsuarioDto usuario;
	
	public DireccionDto() {

		
	}
	
    public DireccionDto (DireccionDto o) {
		
        this.setCalle(o.getCalle()); 	
    	this.setColonia(o.getColonia());
    	this.setCp(o.getCp());
    	this.setFechaCreacion(o.getFechaCreacion());
    	this.setLatitud(o.getLatitud());
    	this.setLongitud(o.getLongitud());
    	this.setNombre(o.getNombre());
   
	}
	
	


	public Integer getIdDireccion() {
		return idDireccion;
	}

	public void setIdDireccion(Integer idDireccion) {
		this.idDireccion = idDireccion;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCp() {
		return cp;
	}

	public void setCp(String cp) {
		this.cp = cp;
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public String getColonia() {
		return colonia;
	}

	public void setColonia(String colonia) {
		this.colonia = colonia;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public double getLatitud() {
		return latitud;
	}

	public void setLatitud(double latitud) {
		this.latitud = latitud;
	}

	public double getLongitud() {
		return longitud;
	}

	public void setLongitud(double longitud) {
		this.longitud = longitud;
	}

	public UsuarioDto getUsuario() {
		return usuario;
	}

	public void setUsuario(UsuarioDto usuario) {
		this.usuario = usuario;
	}	
	
	/**
	 * Relación de muchos a muchos con el despachador
	 * @author José Antonio González Hernández
	 */
	@JsonIgnore
	@ManyToMany(mappedBy = "direcciones")
	private List<DespachadorDto> despachadores;

	/**
	 * @author José Antonio González Hernández
	 */
	public List<DespachadorDto> getDespachadores() {
		return despachadores;
	}
	
	/**
	 * @author José Antonio González Hernández
	 */
	public void setDespachadores(List<DespachadorDto> despachadores) {
		this.despachadores = despachadores;
	}

}
