package com.enermex.controller;

import java.util.List;

import com.enermex.dto.DespachadorDto;
import com.enermex.service.DespachadorService;
import com.enermex.utilerias.RestResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Servicios REST para el Despachador
 * @author José Antonio González Hernández
 * 
 * Contacto: agonzalez@chromaticus.mx
 * Fecha de creación: 30 de diciembre de 2019
 */
@RestController
@RequestMapping("/despachadores")
public class DespachadorController {
    private Logger logger =
        LoggerFactory.getLogger(DespachadorController.class);
    
    @Autowired
    private DespachadorService despachadorService;

    /**
     * Listado de despachadores no eliminados
     * 
     * @return RestResponse con la lista de despachadores
     */
    @GetMapping
    @ResponseBody
    public RestResponse<List<DespachadorDto>> getDespachadores() {
        final RestResponse<List<DespachadorDto>> response = 
            new RestResponse<List<DespachadorDto>>();
        final List<DespachadorDto> despachadores =
            despachadorService.queryAll(false);
        
        if(despachadores != null){
            response.setSuccess(true);
            response.setData(despachadores);
        } else {
            response.setSuccess(false);
            response.setCode("500");
            response.setMeessage(
                "Servicio no disponible. Por favor, inténtelo más tarde");
        }

        return response;
    }

    /**
     * Obtiene un despachador que, por defecto, no debe estar eliminado
     * 
     * @param id Identificador del despachador; se establece como
     *        parte del Path
     * @param deleted Indica si se debe incluir los eliminados; se
     *        establece como parámetro de Url
     * @return RestResponse con el despachador
     */
    @GetMapping("/{id}")
    @ResponseBody
    public RestResponse<DespachadorDto> getDespachador(
            @PathVariable("id") long id,
            @RequestParam(name="deleted", defaultValue="false") boolean deleted
        ) {
        final RestResponse<DespachadorDto> response = 
            new RestResponse<DespachadorDto>();
        DespachadorDto despachador =
            despachadorService.queryById(id, deleted);

        if(despachador != null){
            response.setSuccess(true);
            response.setData(despachador);
        } else {
            response.setSuccess(false);
            response.setCode("REQUEST");
            response.setMeessage(
                "No existe el despachador solicitado");
        }

        return response;
    }

    @PostMapping()
    @ResponseBody
    public RestResponse<Long> postDespachador(
            @RequestBody 
            DespachadorDto despachador) {
        // TODO: Debe realizarse el envío de correo electrónico y/o teléfono además del cálculo de contraseña
        final RestResponse<Long> response = new RestResponse<Long>();

        if(despachadorService.createDespachador(despachador)){
            response.setSuccess(true);
        } else {
            response.setSuccess(false);
            response.setCode("INTERNAL");
            response.setMeessage(
                "Servicio no disponible. Por favor, inténtelo más tarde");
        }

        return response;
    }

    @PutMapping("/{id}")
    @ResponseBody
    public RestResponse<Long> putDespachador(
            @PathVariable("id") long id,
            @RequestBody DespachadorDto despachador) {
        // TODO: Debe realizarse el envío de correo electrónico y/o teléfono además del cálculo de contraseña
        final RestResponse<Long> response = new RestResponse<Long>();

        // TODO: Validar ID
        if(despachadorService.updateDespachador(despachador)){
            response.setSuccess(true);
        } else {
            response.setSuccess(false);
            response.setCode("INTERNAL");
            response.setMeessage(
                "Servicio no disponible. Por favor, inténtelo más tarde");
        }

        return response;
    }

    @DeleteMapping("/{id}")
    @ResponseBody
    public RestResponse<Long> deleteDespachador(
            @PathVariable("id") long id,
            @RequestBody DespachadorDto despachador) {
        // TODO: Debe realizarse el envío de correo electrónico y/o teléfono además del cálculo de contraseña
        final RestResponse<Long> response = new RestResponse<Long>();

        // TODO: Validar ID
        if(despachadorService.deleteDespachador(despachador)){
            response.setSuccess(true);
        } else {
            response.setSuccess(false);
            response.setCode("INTERNAL");
            response.setMeessage(
                "Servicio no disponible. Por favor, inténtelo más tarde");
        }

        return response;
    }
}
