package com.enermex.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.enermex.dto.TipoPlanDto;
import com.enermex.repository.TipoPlanRepository;

@RestController
@RequestMapping("/tipoPlan")
public class TipoPlanController {
	
	
	@Autowired
    private TipoPlanRepository tipoPlanRepository;
	
	
	@GetMapping
	public List<TipoPlanDto> getAll() {
		
		return tipoPlanRepository.findAll();
		
	}
	

}
