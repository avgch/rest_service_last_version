package com.enermex.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.enermex.dto.AutomovilDto;


@RestController
@RequestMapping("/automovil")
public class AutomovilController {
	
	
	
	@PostMapping
	public ResponseEntity<String> save(@RequestBody AutomovilDto auto){
		
		
		try {
			
			//pipaService.saveUpdate(pipa);

		     
    	 }
    	catch(Exception e) {
    		
    		return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    	}
  
    	return new ResponseEntity<>("El registro se agrego correctamente", HttpStatus.OK);
	}
	
	
	
	
	@PutMapping
	@PreAuthorize("hasRole('ROLE_ADMINISTRADOR')")
	public ResponseEntity<String> update(@RequestBody AutomovilDto auto){
		
		
		try {
			
			//pipaService.update(pipa);

		     
    	 }
    	catch(Exception e) {
    		
    		return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    	}
  
    	return new ResponseEntity<>("El registro se agrego correctamente", HttpStatus.OK);
	}
	
	
	@GetMapping
	@PreAuthorize("hasRole('ROLE_ADMINISTRADOR')")
	@ResponseBody
	public List<AutomovilDto> getAll(){
	
		
	try {
//		response.setLista(pipaService.getPipas());
//		response.setCode("200");
//		response.setMessage("Registro exitoso");
//		
//		return new ResponseEntity<Object>(response, HttpStatus.OK);
		//return pipaService.getPipas();
		return null;
    	
    	 }
    	catch(Exception e) {
    		return  null;
    		
//    		response.setCode("500");
//    		response.setMessage(e.getMessage());
//    		return new ResponseEntity<Object>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    	}
  
    	
	}
	
	

}
	