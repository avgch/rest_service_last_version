package com.enermex.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.enermex.service.PipaService;
import com.enermex.dto.ResponseDto;

import com.enermex.dto.PipaDto;
 

@RestController
@RequestMapping("/pipas")
public class PipasController {
	
	
	
	@Autowired
	PipaService pipaService;
	
	@Autowired
	ResponseDto response;
	
	
	@GetMapping
	@PreAuthorize("hasRole('ROLE_ADMINISTRADOR')")
	@ResponseBody
	public List<PipaDto> getPipas(){
	
		
	try {
//		response.setLista(pipaService.getPipas());
//		response.setCode("200");
//		response.setMessage("Registro exitoso");
//		
//		return new ResponseEntity<Object>(response, HttpStatus.OK);
		return pipaService.getPipas();
    	
    	 }
    	catch(Exception e) {
    		return  null;
    		
//    		response.setCode("500");
//    		response.setMessage(e.getMessage());
//    		return new ResponseEntity<Object>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    	}
  
    	
	}
	
	
	
	
    
	@RequestMapping(value = "/pipa", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_ADMINISTRADOR')")
	@ResponseBody
    public ResponseEntity<PipaDto> getPipaById(@RequestParam("id") Integer id) {
    	
    	try {
    		PipaDto pipa = this.pipaService.findPipaById(id);
    		
    		
    		return new ResponseEntity<PipaDto>(pipa, HttpStatus.OK);
    	 }

    	catch(Exception e) {
    
    		return null;
    	}
    
    }
	
	@PostMapping
	public ResponseEntity<String> save(@RequestBody PipaDto pipa){
		
		
		try {
			
			pipaService.saveUpdate(pipa);

		     
    	 }
    	catch(Exception e) {
    		
    		return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    	}
  
    	return new ResponseEntity<>("El usuario se agrego correctamente", HttpStatus.OK);
	}
	
	
	@PutMapping
	@PreAuthorize("hasRole('ROLE_ADMINISTRADOR')")
	public ResponseEntity<String> update(@RequestBody PipaDto pipa){
		
		
		try {
			
			pipaService.update(pipa);

		     
    	 }
    	catch(Exception e) {
    		
    		return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    	}
  
    	return new ResponseEntity<>("El usuario se agrego correctamente", HttpStatus.OK);
	}
	

}
