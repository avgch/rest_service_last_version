package com.enermex.utilerias;

import org.springframework.core.io.Resource;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.Signature;
import java.security.SignatureException;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.Random;

import org.springframework.core.io.ClassPathResource;

/**
 * José Antonio González Hernández
 * 
 * Clase que permite firmar, cifrar y verificar cadenas de URL
 */
public class UrlSigner {
    // TODO: Exponer como servicio de Spring

    private static RSAPublicKey PUBLIC_KEY;
    private static RSAPrivateKey PRIVATE_KEY;

    private static String getPemKey(boolean isPrivate) throws IOException {
        // Lectura del archivo de clave pública o privada en formato PEM
        String resourceName = isPrivate ? "url_private.pem" : "url_public.pem";
        Resource resource = new ClassPathResource(resourceName);
        File file = resource.getFile();
        BufferedReader br = new BufferedReader(new FileReader(file));

        // Los archivos ya no deben contener los marcadores de certificado
        String pem = "";
        String line;
        while ((line = br.readLine()) != null) {
            pem += line;
        }
        br.close();

        return pem;
    }

    private static RSAPublicKey getPublicKey()
            throws IOException, GeneralSecurityException {
        // Obtiene la clave pública RSA; no está en un certificado X509
        if (PUBLIC_KEY != null) {
            return PUBLIC_KEY;
        }

        byte[] encoded = Base64.getDecoder().decode(getPemKey(false));
        KeyFactory kf = KeyFactory.getInstance("RSA");
        X509EncodedKeySpec spec = new X509EncodedKeySpec(encoded);
        PUBLIC_KEY = (RSAPublicKey) kf.generatePublic(spec);
        return PUBLIC_KEY;
    }

    private static RSAPrivateKey getPrivateKey()
            throws IOException, GeneralSecurityException {
        // Obtiene la clave privada; debe estar en formato PKCS8 sin contraseña
        if (PRIVATE_KEY != null) {
            return PRIVATE_KEY;
        }

        byte[] encoded = Base64.getDecoder().decode(getPemKey(true));
        KeyFactory kf = KeyFactory.getInstance("RSA");
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(encoded);
        PRIVATE_KEY = (RSAPrivateKey) kf.generatePrivate(keySpec);
        return PRIVATE_KEY;
    }

    public static String sign(String data) {

        try {
            // Salt sencillo para cambiar la firma
            Random r = new Random();
            String c1 = String.valueOf((char)(r.nextInt(26) + 'a'));
            String c2 = String.valueOf((char)(r.nextInt(26) + 'a'));
            String c3 = String.valueOf((char)(r.nextInt(26) + 'a'));
            String c4 = String.valueOf((char)(r.nextInt(26) + 'a'));
            data = c1 + c2 + c3 + c4 + data;
            
            // Firma de los datos
            Signature sign = Signature.getInstance("SHA1withRSA");
            sign.initSign(getPrivateKey());
            sign.update(data.getBytes("UTF-8"));

            // Se realiza una asignación de los datos
            String dotted = data + "|||"
                + new String(Base64.getEncoder().encode(sign.sign()));

            return new String(
                Base64.getEncoder().encode(dotted.getBytes("UTF-8")));
        } catch (Exception e) {
            e.printStackTrace();

            return null;
        }
    }

    public static String unbox(String data) {
        try {
            String undotted = new String(Base64.getDecoder().decode(data.getBytes()), "UTF-8");
            String[] parts = undotted.split("\\|\\|\\|");

            if (parts.length == 2 && verify(parts[0], parts[1])) {
                return parts[0].substring(4);
            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static boolean verify(String message, String signature) throws GeneralSecurityException, IOException,
            SignatureException, NoSuchAlgorithmException, UnsupportedEncodingException, InvalidKeyException {
        Signature sign = Signature.getInstance("SHA1withRSA");
        sign.initVerify(getPublicKey());
        sign.update(message.getBytes("UTF-8"));
        return sign.verify(Base64.getDecoder().decode(signature));
    }
}
