package com.enermex.utilerias;

import org.springframework.stereotype.Component;

/**
 * Clase para la gestión de errores en peticiones de servicios REST
 * @author José Antonio González Hernández
 * 
 * Contacto: agonzalez@chromaticus.mx
 * Fecha de creación: 30 de diciembre de 2019
 */
@Component
public class RestResponse<T> {
    private boolean success;
    private T data;
    private String code;
    private String meessage;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMeessage() {
		return meessage;
	}

	public void setMeessage(String meessage) {
		this.meessage = meessage;
	}
    
    
    
}
